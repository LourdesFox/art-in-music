# The Beauty of ART in MUSIC

When I hit the button to find a good example of art in music, I stumble upon in huge and colorful art of music of Web Audio API and HTML Canvas. As I observe the audio runs through FFT and it result a unique and magnificent data of art.  See the result below. 

I used the audio of a rock band “Linking Park – Numb” and create a combination of the circumference of shade and light. Now I have to [claim compensation](http://flightclaimcompensation.co.uk) for this art! Kidding. This one of the best MUSIC-DNA I have ever encountered so far. Love the [aerotwist](http://lab.aerotwist.com/canvas/music-dna/). 


![image](http://lab.aerotwist.com/canvas/music-dna/soundna.png)